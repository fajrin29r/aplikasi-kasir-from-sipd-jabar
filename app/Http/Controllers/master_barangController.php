<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class master_barangController extends Controller
{
    public function create(){
    	return view ('master_barang.form_barang');
    }
    public function store(Request $request){

    	// dd($request->all());
    	$request->validate(
            [
                'nama_barang' => 'required',
                'kuantitas' => 'required',
            ],
            [
                'kuantitas.required' => 'Inputan kuantitas harus diisi',
            ]
        );

        DB::table('users')->insert(
                ['email' => 'john@example.com', 'votes' => 0]
            );
    }
}
