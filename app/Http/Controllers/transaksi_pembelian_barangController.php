<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class transaksi_pembelian_barangController extends Controller
{
    public function create(){
    	return view ('transaksi.transaksi_pembelian_barang');
    }
    public function store(Request $request){

    	// dd($request->all());
    	$request->validate(
            [
                'nama_barang' => 'required',
                'kuantitas' => 'required',
            ],
            [
                'kuantitas.required' => 'Inputan kuantitas harus diisi',
            ]
        );

        // DB::table('transaksi_pembelian_barang')->insert(
        //         // 'nama_barang' => $request['nama_barang'],
        //         'jumlah' => $request['kuantitas'],
        //     );
    }

    public function index(){
        $transaksi_pembelian_barang = DB::table('transaksi_pembelian_barang')->get();
 
        return view('transaksi.index', ['transaksi_pembelian_barang' => $transaksi_pembelian_barang]);
    }
}
