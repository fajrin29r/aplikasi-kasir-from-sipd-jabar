<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterBarang extends Model
{
    protected $fillable = [
        'nama_barang', 'harga_satuan',
    ];
}
