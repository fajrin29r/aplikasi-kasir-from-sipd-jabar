<?php

// namespace \Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name'=>'Super Admin',
            'email'=>'info@lucidsitedesigns.com',
            'password'=> bcrypt('123456'),
        ]);
    }
}
