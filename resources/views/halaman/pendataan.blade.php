@extends('layout.master')
@section('judul')
    Ini Halaman Pendataan
@endsection

@section('content')
	<h5>Halaman Pendataan</h5>
	<form action="/kirim" method="post">
		
		@csrf <!-- untuk token -->

		<label>Nama Lengkap</label><br>
		<input type="text" name="nama"><br><br>
		<label>Alamat</label><br>
		<textarea name="alamat" cols="30" rows="10"></textarea><br><br>
		<input type="submit" value="kirim">
	</form>
@endsection