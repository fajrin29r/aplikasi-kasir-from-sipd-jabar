@extends('layout.master')
@section('judul')
    Ini List Harga Barang Satuan
@endsection

@section('content')

<a href="/transaksi/transaksi_pembelian_barang" class="btn btn-primary my-3">Tambah transaksi</a>

<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama_barang</th>
      <th scope="col">Harga_satuan</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  	@forelse ($master_barangs as $key => $item)
  	    <tr>
  	    	<td>{{$key + 1}}</td>
  	    	<td>{{$item->nama_barang}}</td>
  	    	<td>{{$item->harga_satuan}}</td>
  	    	<td>
  	    		<a href="" class="btn btn-info btn-sm">Detail</a>
  	    	</td>
  	    </tr>
  	@empty

  	@endforelse
  </tbody>
</table>

@endsection