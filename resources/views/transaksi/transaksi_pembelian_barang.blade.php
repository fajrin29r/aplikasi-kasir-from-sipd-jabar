@extends('layout.master')
@section('judul')
    Ini Form Barang
@endsection

@section('content')

<form action="/transaksi" method="post">
	@csrf
    
    <div class="form-group">
        <label>Pilih Barang: </label>
        <select name="nama_barang">
        	<option value="Sabun Batang">Sabun Batang</option>
        	<option value="Mi Instan">Mi Instan</option>
        	<option value="Pensil">Pensil</option>
        	<option value="Kopi sachet">Kopi sachet</option>
        	<option value="Air minum galon">Air minum galon</option>
        </select>
    </div>
    
	<br><br><br><br><br><br>
    
      <div class="form-group">
        <label>kuantitas</label>
        <input type="text" class="form-control" name="kuantitas" placeholder="input kuantitas">
      </div>
    @error('kuantitas')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

      <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection