<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/master', function(){
// 	return view('layout.master');
// });

Route::get('/','IndexController@dashboard');
Route::get('/pendaftaran','FormController@pendataan');
Route::post('/kirim','FormController@welcome');


Route::get('/transaksi/transaksi_pembelian_barang', 'transaksi_pembelian_barangController@create');
Route::post('/transaksi', 'transaksi_pembelian_barangController@store');

Route::get('/transaksi','transaksi_pembelian_barangController@index');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
